// class Vechile {
// 	constructor(name) {
// 		this.fullName = name;
// 		this.isDiesel = false;
// 		this.whells = 4;
// 		this._color = "Red";
// 		// const fullName = name;
// 		console.log("Hello", name);
// 	}

// 	greetings(weather) {
// 		console.log("Hello World!!!", this.fullName);
// 		console.log("Today is", weather);
// 	}

// 	static hello() {
// 		console.log("Hola!!");
// 	}

// 	get color() {
// 		return this._color.toUpperCase();
// 	}

// 	set color(x) {
// 		this._color = x[0].toUpperCase() + x.slice(1).toLocaleLowerCase();
// 	}
// }

// const lola = new Vechile("lola");
// // const lola = {
// // 	fullName: "Lola",
// // 	greetings: () => {},
// // };
// // const mcQueen = {
// //   fullName: 'macQueen',
// //   greetings: () => {}
// // }
// // console.log(lola);
// // lola.greetings("Rainy");
// // console.log("Fullname Vechile", lola.fullName);
// // console.log(lola.greetings);

// // Vechile.hello();
// // lola.hello();

// class MotorCycle extends Vechile {
// 	constructor() {
// 		super("Honda");
// 		this.whells = 2;
// 		this.isDiesel = true;
// 	}
// }

// class Car extends Vechile {
// 	constructor() {
// 		super("Ferrari");
// 	}
// }

// // const motor = new MotorCycle();
// // console.log(motor);
// // motor.greetings("Sunny");

// const mobil = new Car();
// mobil.greetings("Rainy");
// // console.log(mobil.color);
// // console.log(mobil._color);

// // mobil.color = "pURple";
// // console.log(mobil._color);
// // mobil.greetings("Windy");

function Clock({ template }) {
	var timer;

	function render() {
		var date = new Date();

		var hours = date.getHours();
		if (hours < 10) hours = "0" + hours;

		var mins = date.getMinutes();
		if (mins < 10) mins = "0" + mins;

		var secs = date.getSeconds();
		if (secs < 10) secs = "0" + secs;

		var output = template
			.replace("h", hours)
			.replace("m", mins)
			.replace("s", secs);

		console.log(output);
	}

	this.stop = function () {
		clearInterval(timer);
	};

	this.start = function () {
		render();
		timer = setInterval(render, 1000);
	};
}

const clock = new Clock({ template: "h:m:s" });
clock.start();
