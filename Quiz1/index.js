// function replaceCharacter(sentence, findCharacter, replaceCharacter) {
// 	var result = "";

// 	for (var index = 0; index < sentence.length; index++) {
// 		const character = sentence[index];

// 		if (character.toLowerCase() === findCharacter.toLowerCase()) {
// 			result += replaceCharacter ?? "";
// 			continue;
// 		}

// 		result += character;
// 	}

// 	return result;
// }

// // // Test Case 1
// var sentence = "Pada Hari Minggu ku turut ayah ke kota";

// var result = replaceCharacter(sentence, "a", "o");
// console.log(result); //Podo Hori Minggu ku turut oyoh ke koto

// // // Test Case 2
// var sentence = "Naik delman istimewa ku duduk di muka";

// var result = replaceCharacter(sentence, "a");
// console.log(result); //Nik delmn istimew ku duduk di muk

// Soal 2
// Format Data [nama, nilai, kelas]
// var studentData = [
// 	[2, "John Duro", 60],
// 	[4, "Robin Ackerman", 100],
// 	[1, "Jaeger Marimo", 60],
// 	[6, "Zoro", 80],
// 	[5, "Zenitsu", 80],
// 	[3, "Patrick Zala", 90],
// ];

function sortGrade(data) {
	data.sort(function (val1, val2) {
		var gradeResult = val2.grade - val1.grade;

		if (gradeResult === 0) {
			return val1.id - val2.id;
		}

		return gradeResult;
	});

	return data;
}
var studentData = [
	{ id: 2, name: "John Duro", grade: 60 },
	{ id: 4, name: "Robin Ackerman", grade: 100 },
	{ id: 1, name: "Jaeger Marimo", grade: 60 },
	{ id: 6, name: "Zoro", grade: 80 },
	{ id: 5, name: "Zenitsu", grade: 80 },
	{ id: 3, name: "Patrick Zala", grade: 90 },
];

var sortedData = sortGrade(studentData);
console.log(sortedData);
// // Output
// // [
// // 	[4, "Robin Ackerman", 100],
// // 	[3, "Patrick", 90],
// // 	[5, "Zenitsu", 80],
// // 	[6, "Zoro", 80],
// // 	[1, "Jaeger Marimo", 60],
// // 	[2, "John Duro", 60],
// // ];
