// var flag = 1;

// console.log("Flag sebelum increment", flag);

// // flag++;
// // ++flag;
// console.log("Flag sesudah increment dibelakang", flag++);
// console.log("Nilai Flag Sekarang", flag);
// console.log("Flag sesudah increment didepan", ++flag);

// Looping Dasar
// var flag = 1;
// while (flag < 10) {
// 	console.log("Iterasi ke -", flag);
// 	flag++;
// }

// console.log("Looping dengan for");
// for (var index = 1; index < 10; index++) {
// 	console.log("Iterasi ke -", index);
// }

// // Kita punya sebuah string "Dag Dig Dug"
// // Bisa melakukan pencarian
// var kata = "Dag Dig Dug";
// // var indexDig = kata.indexOf("i");

// // console.log(indexDig);
// var keyword = "i";
// var indexKeyword;
// for (var index = 0; index < kata.length; index++) {
// 	const karakter = kata[index];
// 	console.log("Iterasi ke-", index);

// 	if (karakter === keyword) {
// 		indexKeyword = index;
// 	}
// }

// console.log({ indexKeyword });
// console.log("Nilai keyword dari kata", kata[indexKeyword]);

// // var bilGenap = 4;
// // var bilGanjil = 3;

// // var sisaBagiBilGenap = bilGenap % 2;
// // var sisaBagiBilGanjil = bilGanjil % 2;

// // console.log({ sisaBagiBilGenap });
// // console.log({ sisaBagiBilGanjil });

// var bilangan = 5;
// var isEven = bilangan % 2 === 0;

// if (isEven) {
// 	console.log(bilangan, "adalah bilaangan genap");
// } else {
// 	console.log(bilangan, "adalah bilaangan ganjil");
// }

// var buffer = "";
// for (let baris = 0; baris < 8; baris++) {
// 	for (let kolom = 0; kolom < 8; kolom++) {
// 		buffer += "#";
// 	}
// 	buffer += "\n";
// }

// console.log(buffer);

// console.log("Looping for dengan continue");
// for (var index = 1; index < 20; index++) {
// 	if (index === 7) {
// 		continue;
// 	}
// 	console.log("Iterasi ke -", index);
// }

// // Ada kalimat "The Break Statement"
// // Hapus huruf e di kalimat
// var kalimat = "The Break Statement";
// var kalimatFiltered = "";
// var filterCharacter = "e";

// for (var index = 0; index < kalimat.length; index += 2) {
// 	if (kalimat[index] === filterCharacter) {
// 		continue;
// 	}

// 	kalimatFiltered += kalimat[index];
// }

// console.log(kalimatFiltered);

// // var kalimat = "The Break Statement";
// var kalimat = "The Break Statement";
// console.log("\nLooping For");
// for (let index = 0; index < kalimat.length; index++) {
// 	const karakter = kalimat[index];

// 	console.log(karakter);
// }

// console.log("\nLooping ForIn");
// for (var index in kalimat) {
// 	var element = kalimat[index];

// 	console.log(index, element);
// }

// console.log("\nLooping ForOf");
// for (var karakter of kalimat) {
// 	console.log(karakter);
// }
