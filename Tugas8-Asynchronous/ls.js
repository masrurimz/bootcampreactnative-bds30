// // setTimeout(() => {
// // 	console.log("Saya diajalankan ke1");
// // 	setTimeout(() => {
// // 		console.log("Saya diajalankan ke2");
// // 		setTimeout(() => {
// // 			console.log("Saya diajalankan ke3");
// // 		}, 3000);
// // 	}, 3000);
// // }, 3000);

// // console.log("Saya dijalankan pertama");

// // // Tunggu ambil data pesan
// // // Tunggu ambil data profil

// // const periksaDokter = require("./periksaDokter");

// // periksaDokter(5, (check, nomorAntri) => {
// // 	if (check) {
// // 		console.log("Sekarang Nomoer", nomorAntri);
// // 		console.log("Sebentar lagi giliran saya");
// // 	} else {
// // 		console.log("Sekarang Nomoer", nomorAntri);
// // 		console.log("Saya jalan-jalan dulu");
// // 	}
// // });

const willIgetNewPhone = (isMomHappy, number) =>
	new Promise((resolve, reject) => {
		setTimeout(() => {
			if (isMomHappy) {
				const phone = {
					number,
					brand: "StangStung",
					color: "black",
				};

				resolve(phone);
			} else {
				const reason = new Error("Mom is not happy");

				reject(reason);
			}
		}, 2000);
	});

// // willIgetNewPhone(true, 1)
// // 	.then((phone) => {
// // 		console.log(phone);
// // 		willIgetNewPhone(true, 2)
// // 			.then((phone) => {
// // 				console.log(phone);
// // 				willIgetNewPhone(true, 2)
// // 					.then((phone) => {
// // 						console.log(phone);
// // 					})
// // 					.catch((reason) => {
// // 						console.log(reason);
// // 					});
// // 			})
// // 			.catch((reason) => {
// // 				console.log(reason);
// // 			});
// // 	})
// // 	.catch((reason) => {
// // 		console.log(reason);
// // 	});

// console.log("Dijalankan");

const runMe = async () => {
	try {
		let res;
		res = await willIgetNewPhone(true, 1);
		console.log(res);

		res = await willIgetNewPhone(true, 2);
		console.log(res);

		res = await willIgetNewPhone(false, 3);
		console.log(res);

		res = await willIgetNewPhone(true, 4);
		console.log(res);
	} catch (error) {
		console.log(error);
	}
};
runMe();

const jumlahkan = (a, b) => a + b;
console.log(jumlahkan(10, 7));

const tampilkan = (x, y, operasi) =>
	`Hasil operasi dari ${x} dan ${y} adalah ${operasi(x, y)}`;
console.log(tampilkan(10, 7, jumlahkan));
console.log(tampilkan(10, 7, (i, j) => i + j));
