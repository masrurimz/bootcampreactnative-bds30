// var deret = [0, 1, 2];
// var hasil = deret.push([3, 4]);

// console.log(deret);
// console.log("Hasil", hasil);

// var deletedElement = deret.pop();
// console.log(deret);
// console.log("Nilai yang dihapus ", deletedElement);

// var teksKosong = "";
// teksKosong += "#";

// console.log(teksKosong);

// var hasil = [];
// for (let index = 0; index < 8; index++) {
// 	hasil.push(index);
// }

// console.log(hasil);

// function buatDeret(panjang) {
// 	var hasil = [];

// 	for (let index = 0; index < panjang; index++) {
// 		hasil.push(index);
// 	}

// 	// console.log(hasil);
// 	return hasil;
// }
// console.log(buatDeret(8));

// // Arr Multidimensi
// var arrMulti = [[0, 1, [20, 30, 40]], [3, 4], [5], 6];
// // console.log(arrMulti[0][2][1]);
// for (let index = 0; index < arrMulti.length; index++) {
// 	const element = arrMulti[index][0][0];

// 	console.log("index", index, element);
// }

// dataSiswa[(nama, nilai, mataPelajaran)];
// var dataSiswa = [
// 	["Ahmad", 60, "Matematika"],
// 	["Naomi", 100, "Matematika"],
// 	["Harun", 80, "Matematika"],
// ];

// console.log("Sebelum Sort", dataSiswa);

// dataSiswa.sort(function (val1, val2) {
// 	if (val1[0] < val2[0]) {
// 		return -1;
// 	}
// 	if (val1[0] > val2[0]) {
// 		return 1;
// 	}

// 	return 0;
// });

// console.log("Sesudah Sort", dataSiswa);

// var angka = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// console.log(angka.slice(1, -1));

// var fruits = ["banana", "orange", "grape"];
// // fruits.splice(1, 1, "Potato", "Dragon Essense");

// // // console.log(fruits);
// // var filteredFruits = fruits.filter(function (value) {
// // 	if (value !== "orange") {
// // 		return true;
// // 	}
// // });

// // console.log("Array asal", fruits);
// // console.log("Array filtered", filteredFruits);

// // dataSiswa[(nama, nilai, mataPelajaran)];
// var dataSiswa = [
// 	["Ahmed", 40, "Matematika"],
// 	["Naomi", 100, "Matematika"],
// 	["Harun", 80, "Matematika"],
// ];

// var siswaPerluJamTambahan = dataSiswa.filter(function (siswa) {
// 	return siswa[1] < 60;
// });

// console.log(siswaPerluJamTambahan);

// // Split
// var fullName = "John Wick Ahmad";
// var nameByWord = fullName.split(" ");

// console.log(nameByWord);

// var firstName = nameByWord[0];
// console.log(firstName);

// var joinedName = nameByWord.join("/");
// console.log(joinedName);

// function contoh(a, b) {
// 	console.log("Nilai a", a);
// 	console.log("Nilai b", b);
// }

// contoh(10, "");

// var x;

// console.log(x);
// console.log(x === undefined, x === "", x === 0);

// console.log(false, !false);
// console.log(Boolean(undefined), !undefined);

// var dataSiswa = [
// 	["Ahmed", 40, "Matematika"],
// 	["Naomi", 100, "Matematika"],
// 	["Harun", 80, "Matematika"],
// ];

// for (let index = 0; index < dataSiswa.length; index++) {
// 	const siswa = dataSiswa[index];

// 	console.log(siswa);
// }

// var hasil = dataSiswa.forEach(function (siswa) {
// 	// console.log(index, "Valuenya ", value, "Arraynya", array);
// 	console.log(siswa);
// });

// console.log(hasil);

// // Nilai dibagi nilai maks * skala baru
// // nilai/4 * 100

// var daftarNilai = [3.4, 3, 3.7, 4.0];

// function koversiKeRange4(listSiswa) {
// 	return listSiswa.map(function (siswa) {
// 		var nilaiRange4 = (siswa[1] / 100) * 4;

// 		siswa[1] = nilaiRange4;

// 		return siswa;
// 	});
// }

// console.log("Skala 4.0 GPA", dataSiswa);
// console.log("Skala 100", koversiKeRange4(dataSiswa));

// function contoh(a, b) {
// 	if (!a || !b) {
// 		return "Bye Byee World !!!";
// 	}

// 	return "Hello World!!";
// }

// console.log(contoh(1, 2));

// var step = 3;
// for (let i = 0; i < 20; i += step) {
// 	console.log(i);
// }
