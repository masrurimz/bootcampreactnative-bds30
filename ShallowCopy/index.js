const state = {
	id: 1,
	person: {
		firstName: "John",
		lastName: "Wick",
		address: {
			city: "",
			province: "",
		},
	},
};

const reducer = (state, action) => {
	switch (action.type) {
		case "SETCITY":
			return {
				...state,
				person: {
					...state.person,
					address: {
						...state.person.address,
						city: action.payload.city,
					},
				},
			};
			break;

		default:
			break;
	}
};

console.log(
	state,
	reducer(state, {
		type: "SETCITY",
		payload: {
			city: "Jakarta",
		},
	}),
);
