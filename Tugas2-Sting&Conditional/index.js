// // Tipe data
// var name = "John"; // Tipe
// var angka = 12.76;
// var todayIsFriday = false;

// console.log(name); // "John"
// console.log(angka); // 12
// console.log(todayIsFriday); // false

// // Operator Aritmatika
// var angka = 12.76;
// var name = "John"; // Tipe
// var todayIsFriday = false;
// var tanpaNilai;

// var b = 10;
// var hasil = tanpaNilai + angka;

// console.log("Nilai dari variable tanpa nilai", tanpaNilai);
// console.log(hasil);

// console.log("Sisa bagi", 6 % 2);

// console.log(!true);

// var angka = 8;
// console.log(angka == "8"); // true, padahal "8" adalah string.
// console.log(angka === "8"); // false, karena tipe data nya berbeda
// console.log(angka === 8); // true

// console.log(Number("90") + 10);
// var stringKosong = "Saya";
// console.log(Boolean(stringKosong));

// var x = 25;
// var y = 100;

// // x = x + y; //Operator Normal
// // console.log(x);

// x /= y; // Operator Assignment
// console.log(x);

// var word = "Javascript is awesome";
// console.log(word.charAt(9)); // 21
// console.log(word[9]); // 21

// var string1 = "good";
// var string2 = "luck";
// var string3 = "sanbers";
// console.log(string1.concat(string2).concat(string3).length); // goodluck
// // console.log("goodluck".concat(string3)); // goodluck
// console.log(string1 + " " + string2 + " " + string3);

// var text = "dung ding ces!";
// console.log(text.indexOf("dung")); // 0
// console.log(text.indexOf("u")); // 1
// console.log(text.indexOf("jreng")); // -1

// // Menambahkan teks "Tak" diantara 2 dung

// // Cari index dari dung
// // Potong jadi 2 bagian
// // Gabungkan
// var indexDing = text.indexOf("ding"); //Lebih mudah dibaca dan dipahami
// var bagianDepan = text.substring(0, indexDing);
// var bagianBelakang = text.substring(indexDing);

// var hasil = bagianDepan + " Tak " + bagianBelakang;
// console.log(hasil);

// var username = " administrator ";
// var newUsername = username.trim();
// console.log(newUsername); // 'administrator'

// var real = "56.7";

// var rounded = Math.round(real);
// var parsed = parseInt(real);

// console.log("Hasil pembulatan", rounded);
// console.log("Hasil parse", parsed);

// console.log("Hello");

// var mood = "fuzzy";
// if (mood.toLowerCase() === "happy".toLowerCase()) {
// 	console.log("hari ini aku bahagia!");
// } else if (mood.toLowerCase() === "fuzzy".toLowerCase()) {
// 	console.log("hari ini aku absurd");
// } else {
// 	console.log("Hari ini aku sedang sedih");
// }

// var a = 5;
// // Jika nilai a habis dibagi 5 maka cetak Hello
// // Jika nilai a bilangan ganjil maka celak World
// // Selain itu cetak Sanbers
// if (a % 2 !== 0) {
// 	console.log("World");
// } else if (a % 5 === 0) {
// 	console.log("Hello");
// } else {
// 	console.log("Sanbers");
// }

// var minimarketStatus = "open";
// var telur = "soldout";
// var buah = "soldout";

// // Menggunakan Nested
// if (minimarketStatus == "open") {
// 	console.log("saya akan membeli telur dan buah");
// 	if (telur == "soldout" || buah == "soldout") {
// 		console.log("belanjaan saya tidak lengkap");
// 	} else if (telur == "soldout") {
// 		console.log("telur habis");
// 	} else if (buah == "soldout") {
// 		console.log("buah habis");
// 	}
// } else {
// 	console.log("minimarket tutup, saya pulang lagi");
// }

// // Tidak Nested
// if ((telur == "soldout" || buah == "soldout") && minimarketStatus == "open") {
// 	console.log("belanjaan saya tidak lengkap");
// } else if (telur == "soldout" && minimarketStatus == "open") {
// 	console.log("telur habis");
// } else if (buah == "soldout" && minimarketStatus == "open") {
// 	console.log("buah habis");
// } else {
// 	console.log("minimarket tutup, saya pulang lagi");
// }

// var a = 5;
// // Jika nilai a habis dibagi 5 maka cetak Hello
// // Jika nilai a bilangan ganjil maka celak World
// // Selain itu cetak Sanbers
// if (a % 2 !== 0) {
// 	if (a % 5 === 0) {
// 		console.log("Hello");
// 	} else {
// 		console.log("World");
// 	}
// } else {
// 	console.log("Sanbers");
// }

// var buttonPushed = "twenty";

// switch (buttonPushed) {
// 	case 1: {
// 		console.log("matikan TV!");
// 		break;
// 	}
// 	case 2: {
// 		console.log("turunkan volume TV!");
// 		break;
// 	}
// 	case 3: {
// 		console.log("tingkatkan volume TV!");
// 		break;
// 	}
// 	case 4: {
// 		console.log("matikan suara TV!");
// 		break;
// 	}
// 	case "five": {
// 		console.log("Tombol 5 diteka");
// 		break;
// 	}
// 	default: {
// 		console.log("Tidak terjadi apa-apa");
// 	}
// }

// var voteable;
// var age = 20;

// if (age < 18) {
// 	voteable = "Too Young";
// } else {
// 	voteable = "Old Enough";
// }

// var voteable =
// 	age < 18 ? "Too young" : age > 100 ? "Your a fossil" : "Old enough";

var name = null;
console.log(Boolean(name));
if (!name) {
	console.log("Bagian true");
} else {
	console.log("Bagian else");
}
// var name = "John ";
// // console.log(!name);
// if (!name) {
// 	console.log("Nama harus diisi!");
// }

// var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
// var bulan = 5; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
// var tahun = 2000; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
