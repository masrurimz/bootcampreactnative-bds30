import React from "react";
import { StyleSheet, Text, View } from "react-native";
import TextInputCustom from "./components/TextInput";

const Tugas12 = () => {
	return (
		<View>
			<TextInputCustom title={"Email/Password"} />
			<TextInputCustom title={"Password"} type={"password"} />
		</View>
	);
};

export default Tugas12;

const styles = StyleSheet.create({});
