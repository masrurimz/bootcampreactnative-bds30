import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 20,
		marginBottom: 20,
	},
	title: {
		fontSize: 16,
		fontWeight: "bold",
		paddingBottom: 10,
	},
	textField: {
		flexDirection: "row",
		borderBottomWidth: 1,
		paddingBottom: 5,
	},
	textInput: {
		flex: 1,
		fontSize: 16,
	},
	iconButton: {
		marginLeft: 10,
	},
});

export default styles;
