import React from "react";
import { StyleSheet, Text, View } from "react-native";

const ItemGreen = ({ title, desctiption }) => (
	<View style={styles.item}>
		<Text style={styles.title}>{title}</Text>
		<Text>{desctiption}</Text>
	</View>
);

const styles = StyleSheet.create({
	item: {
		backgroundColor: "green",
		padding: 20,
		marginVertical: 8,
		marginHorizontal: 16,
	},
	title: {
		fontSize: 32,
	},
});

export default ItemGreen;
