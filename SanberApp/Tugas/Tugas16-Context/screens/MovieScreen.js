import React, { useContext } from "react";
import { MovieContext, MovieProvider } from "../contexts/MovieContext";
import MovieList from "../components/MovieList";
import MovieForm from "../components/MovieForm";

const MovieScreen = () => {
	const { movie } = useContext(MovieContext);

	return (
		<>
			<MovieList />
			<MovieForm />
		</>
	);
};

export default MovieScreen;
