import React from "react";
import { View, Text } from "react-native";
import { MovieProvider } from "./contexts/MovieContext";
import MovieScreen from "./screens/MovieScreen";

const Tugas16 = () => {
	return (
		<MovieProvider>
			<MovieScreen />
		</MovieProvider>
	);
};

export default Tugas16;
