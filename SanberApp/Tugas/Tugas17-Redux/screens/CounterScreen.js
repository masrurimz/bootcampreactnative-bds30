import React, { useEffect } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment, incrementByAmount } from "../redux/counterSlice";
import { getNews, getNewsWithoutThunk } from "../redux/newsSlice";

export default function CounterScreen() {
	const count = useSelector((state) => state.counter.value);
	const news = useSelector((state) => state.news.list);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getNews());
		// dispatch(getNewsWithoutThunk());
	}, []);

	useEffect(() => {
		console.log(news);
	}, [news]);

	return (
		<View>
			<View>
				<Button title="Increment" onPress={() => dispatch(increment())} />
				<Text>{count}</Text>
				<Button title="Decrement" onPress={() => dispatch(decrement())} />
				<Button
					title="Increment By 20"
					onPress={() => dispatch(incrementByAmount(20))}
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({});
