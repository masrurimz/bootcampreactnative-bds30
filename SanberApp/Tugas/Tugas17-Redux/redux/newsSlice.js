import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const baseURL = "https://sanbers-news-api.herokuapp.com/api";

const client = axios.create({
	baseURL: baseURL,
});

export const getNews = createAsyncThunk("news/getNews", async () => {
	const res = await client.get(`/news`);
	const news = res.data.results.news;

	return news;
});

export const addNews = createAsyncThunk(
	"news/getNews",
	async ({ title, value }, thunkAPI) => {
		const res = await client.post(`/news`, { title, value });
	},
);

const initialState = {
	list: [],
	selected: {},
	isLoading: false,
};

const newsSlice = createSlice({
	name: "news",
	initialState,
	reducers: {
		// getNewsWithoutThunk: (state) => {
		// 	const _getNews = async () => {
		// 		const res = await client.get(`/news`);
		// 		const news = res.data.results.news;
		// 		state.list = news;
		// 	};
		// 	_getNews();
		// },
	},
	extraReducers: (builder) => {
		builder.addCase(getNews.fulfilled, (state, action) => {
			state.isLoading = false;
			state.list = action.payload;
		});
		builder.addCase(getNews.rejected, (state, action) => {
			state.isLoading = false;
		});
		builder.addCase(getNews.pending, (state, action) => {
			state.isLoading = true;
		});
	},
});

export const {} = newsSlice.actions;
export default newsSlice.reducer;
