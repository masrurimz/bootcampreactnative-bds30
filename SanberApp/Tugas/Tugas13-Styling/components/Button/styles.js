import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	button: {
		alignItems: "center",
		borderRadius: 10,
		padding: 10,
		marginBottom: 15,
	},
	buttonFilled: {
		backgroundColor: "blue",
	},
	buttonOutline: {
		backgroundColor: "white",
		borderWidth: 1,
		borderColor: "blue",
	},
	buttonTitle: {
		color: "white",
		fontWeight: "bold",
		fontSize: 18,
	},
	buttonTitleFilled: {
		color: "white",
	},
	buttonTitleOutlined: {
		color: "blue",
	},
});

export default styles;
