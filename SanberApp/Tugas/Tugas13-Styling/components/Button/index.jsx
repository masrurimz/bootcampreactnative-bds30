import React from "react";
import { Text, TouchableOpacity } from "react-native";
import styles from "./styles";

const ButtonCustom = ({ title, type = "filled" }) => {
	console.log(style);
	return (
		<TouchableOpacity
			style={[
				styles.button,
				styles.buttonFilled,
				type === "outlined" && styles.buttonOutline,
				style,
			]}>
			<Text
				style={[
					styles.buttonTitle,
					styles.buttonTitleFilled,
					type === "outlined" && styles.buttonTitleOutlined,
					styleTitle,
				]}>
				{title}
				{/* Login */}
			</Text>
		</TouchableOpacity>
	);
};

export default ButtonCustom;
