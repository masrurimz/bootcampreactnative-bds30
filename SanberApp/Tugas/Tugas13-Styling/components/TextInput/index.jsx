import React, { useState } from "react";
import { View, Text, TextInput, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";

import styles from "./styles";

const TextInputCustom = ({ title, type, style = {}, styleTitle = {} }) => {
	const [isSecured, setIsSecured] = useState(
		type === "password" ? true : false,
	);

	const toggleSecured = () => setIsSecured((_isSecure) => !_isSecure);

	return (
		<View style={styles.container}>
			<Text style={styles.title}>{title}</Text>
			<View style={[styles.textField, style]}>
				<TextInput style={styles.textInput} secureTextEntry={isSecured} />
				{type === "password" && (
					<TouchableOpacity style={styles.iconButton} onPress={toggleSecured}>
						<AntDesign name="eyeo" size={24} color="black" />
					</TouchableOpacity>
				)}
			</View>
		</View>
	);
};

export default TextInputCustom;
