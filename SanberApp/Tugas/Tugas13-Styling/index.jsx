import React from "react";
import {
	ScrollView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from "react-native";
import {
	useFonts,
	Inter_900Black,
	Inter_400Regular,
} from "@expo-google-fonts/inter";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import ButtonCustom from "./components/Button";
import TextInputCustom from "./components/TextInput";

const Flex = () => {
	return (
		<ScrollView style={styles.container}>
			<Text style={styles.header}>Register</Text>
			<View style={styles.form}>
				<TextInputCustom
					title={"Email"}
					style={{ borderBottomColor: "blue", borderBottomWidth: 5 }}
				/>
				<TextInputCustom title={"Password"} type={"password"} />
				<TextInputCustom title={"Password"} type={"password"} />
			</View>
			<View style={styles.buttonContainer}>
				<ButtonCustom title={"Login"} />
				<ButtonCustom title={"Register"} type="outlined" />
			</View>
			<View style={styles.footer}>
				<Text style={styles.footerText}>Footer</Text>
				<MaterialCommunityIcons name="gitlab" size={48} color="black" />
			</View>
			<View
				style={{
					flexDirection: "row",
					justifyContent: "space-between",
				}}>
				<View
					style={{
						padding: 20,
						backgroundColor: "red",
						transform: [
							{
								rotate: "45deg",
							},
						],
					}}
				/>
				<View style={{ padding: 20, backgroundColor: "blue" }} />
			</View>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// flexDirection: "column-reverse",
	},
	header: {
		paddingHorizontal: 20,
		paddingVertical: 10,
		fontSize: 18,
		borderBottomWidth: 1,
		marginBottom: 20,
	},
	form: {
		flex: 1,
	},
	buttonContainer: {
		// position: "absolute",
		// bottom: 0,
		// right: 0,
		paddingHorizontal: 20,
	},

	footer: {
		paddingHorizontal: 20,
		alignItems: "center",
	},
	footerText: {
		// fontFamily: "Inter_400Regular",
		fontSize: 18,
	},
});

export default Flex;
