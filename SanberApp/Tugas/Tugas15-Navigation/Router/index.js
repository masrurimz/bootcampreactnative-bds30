import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";

import HomeScreen from "../Screen/HomeScreen";
import AboutScreen from "../Screen/AboutScreen";
import LoginScreen from "../Screen/LoginScreen";
import ProjectScreen from "../Screen/ProjectScreen";
import SettingScreen from "../Screen/SettingScreen";
import SkillScreen from "../Screen/SkillScreen";
import AddScreen from "../Screen/AddScreen";
import SplashScreen from "../Screen/SplashScreen";
import RegisterScreen from "../Screen/RegisterScreen";

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
	return (
		<NavigationContainer>
			<Stack.Navigator screenOptions={{ headerShown: false }}>
				<Stack.Screen name="LoginScreen" component={LoginScreen} />
				<Stack.Screen name="HomeScreen" component={HomeScreen} />
				<Stack.Screen name="MainApp" component={MainApp} />
				<Stack.Screen name="MyDrawer" component={MyDrawer} />
			</Stack.Navigator>
		</NavigationContainer>
	);
}

const MainApp = () => (
	<Tab.Navigator screenOptions={{ headerShown: false }}>
		<Tab.Screen name="HomeScreen" component={HomeScreen} />
		<Tab.Screen name="AddScreen" component={AddScreen} />
		<Tab.Screen name="SkillScreen" component={SkillScreen} />
	</Tab.Navigator>
);

const MyDrawer = () => (
	<Drawer.Navigator>
		<Drawer.Screen name="App" component={MainApp} />
		<Drawer.Screen name="AboutScreen" component={AboutScreen} />
	</Drawer.Navigator>
);
