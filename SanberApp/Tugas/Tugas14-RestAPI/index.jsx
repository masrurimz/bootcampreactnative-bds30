import React, { useEffect, useState } from "react";
import { Alert, Button, View, Text, TextInput, StyleSheet } from "react-native";
import axios from "axios";

const validatePassword = (password = "") => {
	if (password.trim() === "") {
		return "New Password should be filled";
	} else {
		if (password.length < 6) {
			return "Should contain 6-20 characters consist of number and alphabet";
		} else {
			return "";
		}
	}
};

const getToken = () => {
	return "Ini Token";
};

const client = axios.create({
	baseURL: "https://sanbers-news-api.herokuapp.com/api/",
	headers: {
		Authorization: `Bearer ${getToken()}`,
	},
});

const getNews = async (setNews) => {
	try {
		const res = await client.get("news");
		// console.log(res.data);
		setNews(res.data);
	} catch (error) {
		console.error(errr);
		Alert.alert("Something wrong", error);
	}
};

const App = () => {
	const [news, setNews] = useState([]);

	useEffect(() => {
		getNews(setNews);
	}, []);

	useEffect(() => {
		console.log("Dari news", news);
	}, [news]);
	// Deklarasi variabel state baru yang kita sebut "count"
	// let counter = 10;
	// const [count1, setCount1] = useState(100000);
	// const [count2, setCount2] = useState(100000);

	// const incrementCounter1 = () => setCount1((_count) => _count + 10);
	// const incrementCounter2 = () => setCount2((_count) => _count + 10);

	// // Dijalankan hanya ketika count1 berubah
	// useEffect(() => {
	// 	console.log("count1", count1);
	// }, [count1]);

	// // Dijalankan hanya sekali
	// useEffect(() => {
	// 	console.log("Saya dijalankan sekali");
	// 	console.log("Mengambil data");
	// }, []);

	// // Dijalankan ketika layarnya hidden karena pindah halaman
	// useEffect(() => {
	// 	const timer = setInterval(() => {
	// 		setCount2((_count) => ++_count);
	// 	}, 1000);

	// 	return () => {
	// 		clearInterval(timer);
	// 	};
	// }, []);

	// const incrementCounter = () => {
	// 	// counter++;
	// 	// console.log("counter", counter);
	// };

	// const [formData, setFormData] = useState({
	// 	password: "",
	// 	company: "",
	// });
	// const error = validatePassword(formData.password);
	// const [name, setName] = useState("");
	// const [company, setCompany] = useState("");

	return (
		<View style={{ justifyContent: "center", flex: 1, paddingHorizontal: 16 }}>
			{/* <Text>Counter 1</Text>
			<Button onPress={incrementCounter1} title="Tambah" />
			<Text style={{ alignItems: "center" }}>
				Anda menekan sebanyak {count1} kali
			</Text>
			<Button onPress={() => setCount1((value) => value - 1)} title="Kurang" />
			<Button onPress={() => setCount1(99)} title="Set Ke 99" />
			<View style={{ height: 40 }} />

			<Text>Counter 2</Text>
			<Button onPress={incrementCounter2} title="Tambah" />
			<Text style={{ alignItems: "center" }}>
				Anda menekan sebanyak {count2} kali
			</Text>
			<Button onPress={() => setCount2((value) => value - 1)} title="Kurang" />
			<Button onPress={() => setCount2(99)} title="Set Ke 99" /> */}

			{/* <Text>
				Password {formData.password} ; Error {error}
			</Text>
			<TextInput
				style={styles.textInput}
				value={formData.password}
				onChangeText={(text) =>
					setFormData((_formData) => ({
						..._formData,
						password: text,
					}))
				}
			/>
			<Text>Company {formData.company}</Text>
			<TextInput
				style={styles.textInput}
				value={formData.company}
				onChangeText={(text) =>
					setFormData((_formData) => ({
						..._formData,
						company: text,
					}))
				}
			/> */}
		</View>
	);
};

export default App;

const styles = StyleSheet.create({
	textInput: {
		borderBottomWidth: 1,
	},
});
