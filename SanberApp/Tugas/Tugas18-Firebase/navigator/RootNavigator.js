import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from "../screens/Auth/LoginScreen";
import Register from "../screens/Auth/RegisterScreen";
import Home from "../screens/HomeScreen";

const Stack = createNativeStackNavigator();

const RootNavigator = () => {
	return (
		<Stack.Navigator initialRouteName="Login">
			<Stack.Screen
				name="Login"
				component={Login}
				options={{ headerShown: false }}
			/>
			<Stack.Screen
				name="Register"
				component={Register}
				options={{ headerShown: false }}
			/>
			<Stack.Screen
				name="Home"
				component={Home}
				options={{ headerTitle: "Daftar Barang" }}
			/>
		</Stack.Navigator>
	);
};

export default RootNavigator;
