import React from "react";
import { NavigationContainer } from "@react-navigation/native";
// Import the functions you need from the SDKs you need
import { initializeApp, getApps } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import RootNavigator from "./navigator/RootNavigator";

// Your web app's Firebase configuration
// const firebaseConfig = {
// 	apiKey: "AIzaSyDzaKtAiTTuW5HaXIRHnAgObEC4ajjH3BA",
// 	authDomain: "bds-30.firebaseapp.com",
// 	projectId: "bds-30",
// 	storageBucket: "bds-30.appspot.com",
// 	messagingSenderId: "228224329043",
// 	appId: "1:228224329043:web:df90237408d7ad2a46ada6",
// };

// // Initialize Firebase
// if (!getApps().length) {
// 	initializeApp(firebaseConfig);
// }

export default function Tugas18() {
	return (
		<NavigationContainer>
			<RootNavigator />
		</NavigationContainer>
	);
}
