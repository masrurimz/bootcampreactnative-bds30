import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";
import { getAuth, onAuthStateChanged } from "firebase/auth";

import Login from "./Login";
import Home from "./Home";
import Register from "./Register";

const Stack = createStackNavigator();

export default function index() {
	const [isSignedIn, setIsSignedIn] = useState(false);

	useEffect(() => {
		const auth = getAuth();
		onAuthStateChanged(auth, (user) => {
			if (user) {
				// User is signed in, see docs for a list of available properties
				// https://firebase.google.com/docs/reference/js/firebase.User
				const uid = user.uid;
				if (uid) {
					setIsSignedIn(true);
				}
				// ...
			} else {
				// User is signed out
				// ...
				setIsSignedIn(false);
			}
		});
	}, []);
	return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName="Login">
				{!isSignedIn ? (
					<>
						<Stack.Screen
							name="Login"
							component={Login}
							options={{ headerShown: false }}
						/>
						<Stack.Screen
							name="Register"
							component={Register}
							options={{ headerShown: false }}
						/>
					</>
				) : (
					<Stack.Screen
						name="Home"
						component={Home}
						options={{ headerTitle: "Daftar Barang" }}
					/>
				)}
			</Stack.Navigator>
		</NavigationContainer>
	);
}
