import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button } from "react-native";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { Alert } from "react-native";

export default function Register({ navigation }) {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isError, setIsError] = useState(false);

	const submit = async () => {
		try {
			const auth = getAuth();
			const userCredential = await createUserWithEmailAndPassword(
				auth,
				email,
				password,
			);
			// Signed in
			const user = userCredential.user;
			Alert.alert(
				"Successfully Register",
				`User with email ${user.email} has been registered`,
			);
			console.log(user);
			// ...
		} catch (error) {
			const errorCode = error.code;
			const errorMessage = error.message;
			Alert.alert("Failed register user", errorMessage);
		}

		// const Data = {
		// 	username,
		// 	password,
		// };
		// console.log(Data);
		// if (password === "12345678") {
		// 	setIsError(false);
		// 	console.log("Login Benar");
		// 	navigation.navigate("Home", {
		// 		username: username,
		// 	});
		// } else {
		// 	console.log("Login Salah");
		// 	setIsError(true);
		// }
	};
	return (
		<View style={styles.container}>
			<Text style={{ fontSize: 20, fontWeight: "bold" }}>== Register ==</Text>
			<Image
				style={{ height: 150, width: 150 }}
				source={require("./assets/logo.jpg")}
			/>
			<View>
				<TextInput
					style={{
						borderWidth: 1,
						paddingVertical: 10,
						borderRadius: 5,
						width: 300,
						marginBottom: 10,
						paddingHorizontal: 10,
					}}
					placeholder="Masukan Email"
					value={email}
					onChangeText={(value) => setEmail(value)}
				/>
				<TextInput
					style={{
						borderWidth: 1,
						paddingVertical: 10,
						borderRadius: 5,
						width: 300,
						marginBottom: 10,
						paddingHorizontal: 10,
					}}
					placeholder="Masukan Password"
					value={password}
					onChangeText={(value) => setPassword(value)}
				/>
				<Button onPress={submit} title="Register" />
				<Button onPress={() => navigation.navigate("Login")} title="Login" />
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
	},
});
