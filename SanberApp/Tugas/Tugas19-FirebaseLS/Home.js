import { DrawerItem } from "@react-navigation/drawer";
import React, { useEffect } from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { interpolate } from "react-native-reanimated";
import { collection, addDoc, getFirestore, getDocs } from "firebase/firestore";
import { getAuth, signOut } from "firebase/auth";

import { Data } from "./data";

export default function Home({ route, navigation }) {
	const email = "";
	const [totalPrice, setTotalPrice] = useState(0);

	const currencyFormat = (num) => {
		return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
	};

	const addToCart = async (price, item) => {
		try {
			console.log("UpdatPrice : " + price);
			const temp = Number(price) + totalPrice;
			console.log(temp);
			setTotalPrice(temp);

			const db = getFirestore();
			const docRef = await addDoc(collection(db, "cart"), item);
			console.log("Document written with ID: ", docRef.id);
			const querySnapshot = await getDocs(collection(db, "cart"));
			console.log("snapshoot", querySnapshot);
		} catch (e) {
			console.error("Error adding document: ", e);
		}
	};
	useEffect(() => {
		console.log("yang dicetak params:", email);
	}, []);

	const logOut = () => {
		const auth = getAuth();
		signOut(auth);
	};

	return (
		<View style={styles.container}>
			<View
				style={{
					flexDirection: "row",
					justifyContent: "space-between",
					padding: 16,
				}}>
				<View>
					<Text>Selamat Datang,</Text>
					<Text style={{ fontSize: 18, fontWeight: "bold" }}>{email}</Text>
				</View>
				<View>
					<Button onPress={logOut} title="LogOut" />
					<Text>Total Harga:</Text>
					<Text style={{ fontSize: 18, fontWeight: "bold" }}>
						{" "}
						{currencyFormat(totalPrice)}
					</Text>
				</View>
			</View>
			<View
				style={{ alignItems: "center", marginBottom: 20, paddingBottom: 60 }}>
				<FlatList
					data={Data}
					numColumns={2}
					keyExtractor={(item) => item.id}
					showsVerticalScrollIndicator={false}
					renderItem={({ item }) => {
						return (
							<View style={styles.content}>
								<Text>{item.title}</Text>
								<Image
									style={{ width: 120, height: 100, alignSelf: "center" }}
									source={item.image}
								/>
								<Text>{item.harga}</Text>
								<Text>{item.type} </Text>
								<Button
									onPress={() => addToCart(item.harga, item)}
									title="Beli"
								/>
							</View>
						);
					}}
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
	},
	content: {
		width: 150,
		height: 220,
		margin: 5,
		borderWidth: 1,
		alignItems: "center",
		borderRadius: 5,
		borderColor: "grey",
	},
});
