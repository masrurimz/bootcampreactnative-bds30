import React, { useState } from "react";
import {
	Image,
	StyleSheet,
	Text,
	View,
	TextInput,
	Button,
	Alert,
} from "react-native";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

export default function Login({ navigation }) {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isError, setIsError] = useState(false);

	const submit = async () => {
		try {
			const auth = getAuth();
			const userCredential = await signInWithEmailAndPassword(
				auth,
				email,
				password,
			);
			const user = userCredential.user;
			navigation.navigate("Home", {
				email: user.email,
			});
		} catch (error) {
			const errorCode = error.code;
			const errorMessage = error.message;
			Alert.alert("Failed to login", errorMessage);
		}
		// const Data = {
		// 	username,
		// 	password,
		// };
		// console.log(Data);

		// if (password === "12345678") {
		// 	setIsError(false);
		// 	console.log("Login Benar");
		// 	navigation.navigate("Home", {
		// 		username: username,
		// 	});
		// } else {
		// 	console.log("Login Salah");
		// 	setIsError(true);
		// }
	};
	return (
		<View style={styles.container}>
			<Text style={{ fontSize: 20, fontWeight: "bold" }}>== Login ==</Text>
			<Image
				style={{ height: 150, width: 150 }}
				source={require("./assets/logo.jpg")}
			/>
			<View>
				<TextInput
					style={{
						borderWidth: 1,
						paddingVertical: 10,
						borderRadius: 5,
						width: 300,
						marginBottom: 10,
						paddingHorizontal: 10,
					}}
					placeholder="Masukan Email"
					value={email}
					onChangeText={(value) => setEmail(value)}
				/>
				<TextInput
					style={{
						borderWidth: 1,
						paddingVertical: 10,
						borderRadius: 5,
						width: 300,
						marginBottom: 10,
						paddingHorizontal: 10,
					}}
					placeholder="Masukan Password"
					value={password}
					onChangeText={(value) => setPassword(value)}
				/>
				<Button onPress={submit} title="Login" />
				<Button
					onPress={() => navigation.navigate("Register")}
					title="Register"
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
	},
});
