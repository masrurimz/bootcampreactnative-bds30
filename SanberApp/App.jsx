import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Tugas12 from "./Tugas/Tugas12-IntroRN";
import Tugas13 from "./Tugas/Tugas13-Styling";
import Tugas14 from "./Tugas/Tugas14-RestAPI";
import Tugas15 from "./Tugas/Tugas15-Navigation";
import { NavigationContainer } from "@react-navigation/native";
import RestApi from "./Tugas/Tugas14-RestAPI/RestApi";
import Tugas16 from "./Tugas/Tugas16-Context";
import Tugas19 from "./Tugas/Tugas19-FirebaseLS";
import Tugas18 from "./Tugas/Tugas18-Firebase";
// import MovieListItem, { Cat, Dog } from "./MovieList";
// Import the functions you need from the SDKs you need
import { initializeApp, getApps } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyD6fcuKpiDXxjA_msOZ8zSAVTFZIEO4tQY",
	authDomain: "bds30-2f1f4.firebaseapp.com",
	projectId: "bds30-2f1f4",
	storageBucket: "bds30-2f1f4.appspot.com",
	messagingSenderId: "1077211216850",
	appId: "1:1077211216850:web:61c1a30bed14ecb583260b",
};

// Initialize Firebase
if (!getApps().length) {
	initializeApp(firebaseConfig);
}

export default function App() {
	return (
		// <View style={styles.container}>
		// 	<Text>Open up App.js to start working on your app!</Text>
		// 	<Text>Open up App.js to start working on your app!</Text>
		// 	<Text>Hello Sanbers</Text>
		// 	<MovieListItem author="Eichiro Oda" name="One Piece" />
		// 	<MovieListItem author="Naruto" name=" Masashi Kisimoto" />
		// 	<Cat />
		// 	<Dog />
		// {/* </View> */}
		<>
			<StatusBar style="light" translucent={false} />
			{/* <Tugas12 /> */}
			{/* <Tugas14 /> */}
			{/* <NavigationContainer>
			</NavigationContainer> */}
			{/* <Tugas15 /> */}
			{/* <RestApi /> */}
			{/* <Tugas16 /> */}
			{/* <Tugas19 /> */}
			<Tugas19 />
		</>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center",
	},
});
