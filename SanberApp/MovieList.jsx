import React from "react";
import { Text, View } from "react-native";

const MovieListItem = ({ name, author }) => {
	return (
		<View>
			<Text>{name}</Text>
			<Text>Author : {author}</Text>
		</View>
	);
};

const Cat = () => <Text>Cat</Text>;
const Dog = () => <Text>Dog</Text>;

export default MovieListItem;
export { Cat, Dog };
