// // // Ada kalimat "The Break Statement"
// // // Hapus huruf e di kalimat
// var kalimat = "The Break Statement";
// var filterCharacter = "e";

// function hapusKarakter(karakterYangDihapus, kalimat) {
// 	var kalimatFiltered = "";

// 	for (var index = 0; index < kalimat.length; index += 1) {
// 		if (kalimat[index] === karakterYangDihapus) {
// 			continue;
// 		}

// 		kalimatFiltered += kalimat[index];
// 	}

// 	return kalimatFiltered;
// }

// var varHapusKarakter = hapusKarakter;

// console.log("Deklarasi", hapusKarakter);
// console.log("Expression/Assignment", varHapusKarakter);

// console.log(hapusKarakter("e", kalimat));
// console.log(hapusKarakter("t", kalimat));

// console.log(varHapusKarakter("e", kalimat));
// console.log(varHapusKarakter("t", kalimat));

// function greetings() {
// 	return console.log("Hello !!!");
// }

// function tampilkan() {
// 	console.log("halo!");

// 	return greetings();
// }

// var kembalian = tampilkan();
// console.log(kembalian);

var bil1 = 10;
var bil2 = 5;

function jumlahkan(a, b) {
	return a + b;
}

function kurangkan(a, b) {
	return a - b;
}

// Diproses dahulu
// function name(params) {

// }

function jalankan(x, y, operasi) {
	var hasil = operasi(x, y);

	return "Hasil dari operasi " + x + " dan " + y + " adalah " + hasil;
}

// console.log(
// 	jalankan(bil1, bil2, function (a, b) {
// 		return a * b;
// 	}),
// );

// Kita Handle Gesture Touch
// Saat tap kita ingin panggil fungsi console.log('Saya di tap')
// var gesturHanlder = console.log('Saya di tap')
// Ada 2 orang, A dan B.
// A memberi tugas ke B, kalau nanti dipanggil maka jalankan gestureHandler
// A ingin agar B saat dipanggil menghitung jumlah dari nilai yang dikirim dan menuliskannya

// var gesturHandler = function (a, b) {
// 	console.log("Saya di tap");

// 	console.log(jumlahkan(a, b));
// };

// var gesturHandler = function (a, b) {
// 	console.log("Saya di tap");

// 	console.log(jumlahkan(a, b));
// };
// // var gesturHandler = function () {
// // 	console.log("Saya di tap");
// // };
// // console.log(gesturHandler);

// console.log(gesturHandler(10, 7));

// function jumlahkan(a, b, c, d = 1) {
// 	return a + b + c + d;
// }

// var hasil = jumlahkan(1, 5);
// console.log(hasil);

// var kalimat = "The Break Statement";
// var hasil = kalimat.substr(4, 5);

// console.log(hasil);

// How to Define Modular/Reusable Function
// Dengan menggunakan pure function
// If a function given a then it will always output b
// Practice
// 1. Function should only retrieve value from parameter
// 2. Function should only give output from return
// 3. Function shouldn't modify parameter outside function
var a = 10;
var b = 7;

function kalikan(x, y, log) {
	log(x * y);
}

kalikan(a, b, console.error);
