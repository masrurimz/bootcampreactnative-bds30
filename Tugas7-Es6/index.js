// var nama = "hilmy";

// {
// 	var nama = "firdaus";
// 	console.log("Di dalam block", nama);
// }

// console.log("Di luar block", nama);

const person = {
	id: 1,
	name: "Hilmy",
};

// Error tidak boleh
// person = {
// 	id: 2,
// 	name: "Firdaus",
// };
person.name = "Ahmad";

console.log(person);

// function sayHiBiasa() {
// 	return "Hello";
// }

// const sayHiArrow = () => "Hello";

// console.log("Biasa", sayHiBiasa());
// console.log("Arrow", sayHiArrow());

// const jumlahkan = (a, b) => a + b;
// const tampilkan = (...props) => {
// 	console.log(props);
// 	console.log(jumlahkan(...props));
// };

// tampilkan(10, 7);

// const data1 = [1, 2, 3, [40, 50, 60]];
// const data2 = [5, 6, 7, 8];

// const combinedData = [...data1, ...data2];
// // console.log(combinedData);

// const car1 = {
// 	brand: "Ford",
// 	color: "White",
// };

// const car2 = {
// 	hp: 1000,
// 	owner: "Linus",
// 	color: "Red",
// };

// const carCombined = {
// 	...car1,
// 	...car2,
// 	wheels: "RH Performance",
// 	hp: 99,
// };

// console.log(carCombined);

// const { hp, color } = carCombined;
// console.log(hp, color);

// const fruits = ["Apple", "Manggo", "Pinapple", "WaterMelon"];
// // console.log("Combined Data");
// const [, mangga, , waterMellon] = fruits;
// console.log(mangga, waterMellon);

// const terimaObject = ({ value, index, arr }) =>
// 	console.log({ value, index, arr });
// terimaObject({
// 	arr: fruits,
// });
